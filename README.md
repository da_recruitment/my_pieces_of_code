# Analytical skills
MySQL + Python in Data Analysis + Pandas + NumPy + Matplotlib + Plotly + ML + GIT + PowerBI + DAX + MS Excel + Digital marketing

## My recent projects portfolio 
* [Python - Classes, OOP](https://gitlab.com/da_recruitment/my_pieces_of_code/-/blob/main/_notebooks/Python%20-%20zadnia%20zaliczeniowe%20rozszerzone.ipynb)
* [Python / Pandas - basics](https://gitlab.com/da_recruitment/my_pieces_of_code/-/blob/main/_notebooks/Pandas%20-%20zadania%20zaliczeniowe.ipynb)
* [Python - basics](https://gitlab.com/da_recruitment/my_pieces_of_code/-/blob/main/_notebooks/Python%20-%20zadnia%20zaliczeniowe%201-15.ipynb)
* [PowerBI reports](https://gitlab.com/da_recruitment/my_pieces_of_code/-/tree/main/_PowerBI_reports)
* [SQL - basics](https://gitlab.com/da_recruitment/my_pieces_of_code/-/blob/main/_SQL/SQL%20-%20PROJEKT_V_FINAL.sql)

## Data
* [Data used in cases are stored here](https://gitlab.com/da_recruitment/my_pieces_of_code/-/tree/main/data)

## Requirements:
* Python 3.11.5 + adequate librairies
* Workbench 8.0 CE or similar