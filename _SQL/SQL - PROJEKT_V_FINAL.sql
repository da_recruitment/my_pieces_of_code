-- Projekt 1 - SQL
-- 1. Stwórz Bazę „Sklep odzieżowy”

CREATE DATABASE sklep_odziezowy;
USE sklep_odziezowy;

-- 2. Utwórz tabelę „Producenci” z kolumnami:

-- id producenta
-- nazwa producenta
-- adres producenta
-- nip producenta
-- data podpisania umowy z producentem
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE producenci (
	producent_id INTEGER PRIMARY KEY,
    nazwa_producenta TEXT,
    adres_producenta TEXT,
    nip_producenta DECIMAL(10,0),
    data_umowy_z_producentem DATE
    );

-- 3. Utwórz tabelę „Produkty” z kolumnami:

-- id produktu
-- id producenta
-- nazwa produktu
-- opis produktu
-- cena netto zakupu
-- cena brutto zakupu
-- cena netto sprzedaży
-- cena brutto sprzedaży
-- procent VAT sprzedaży
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE produkty (
	produkt_id INTEGER PRIMARY KEY,
    producent_id INTEGER,
    nazwa_produktu TEXT,
    opis_produktu TEXT,
    cena_netto_zakupu DECIMAL(10,2),
    cena_brutto_zakupu DECIMAL(10,2),
    cena_netto_sprzedazy DECIMAL(10,2),
    procent_VAT_sprzedazy DECIMAL(10,4),
	data_umowy_z_producentem DATE
    );

-- 4. Utwórz tabelę „Zamówienia” z kolumnami:

-- id zamówienia
-- id klienta
-- id produktu
-- Data zamówienia
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE zamowienia (
    zamowienie_id INTEGER PRIMARY KEY,
    klient_id INTEGER,
    produkt_id INTEGER,
	Data_zamowienia DATE
	);

-- 5. Utwórz tabelę „Klienci” z kolumnami:

-- id klienta
-- id zamówienia
-- imię
-- nazwisko
-- adres
-- Do każdej kolumny ustaw odpowiedni „constraint”

CREATE TABLE klienci (
    klient_id INTEGER PRIMARY KEY,
    zamowienie_id INTEGER,
    imie TEXT,
    nazwisko TEXT,
    adres_klienta TEXT
	);

-- 6. Połącz tabele ze sobą za pomocą kluczy obcych:

-- Produkty – Producenci

ALTER TABLE produkty
	ADD CONSTRAINT FK_producenta 
    FOREIGN KEY (producent_id)
    REFERENCES producenci(producent_id) 
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- Zamówienia – Produkty

ALTER TABLE zamowienia
	ADD CONSTRAINT FK_produktu 
    FOREIGN KEY (produkt_id)
    REFERENCES produkty(produkt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- Zamówienia - Klienci

ALTER TABLE zamowienia
	ADD CONSTRAINT FK_klienta 
    FOREIGN KEY (klient_id)
    REFERENCES klienci(klient_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- --- ZROBIŁEM DODATKOWE POWIĄZANIE i UTRUDNIŁEM SOBIE PRACĘ DALEJ ;)

ALTER TABLE klienci
	ADD CONSTRAINT FK_zamowienia 
    FOREIGN KEY (zamowienie_id)
    REFERENCES zamowienia(zamowienie_id) 
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- 7. Każdą tabelę uzupełnij danymi wg:

-- Tabela „Producenci” – 4 pozycje

INSERT INTO producenci (producent_id, nazwa_producenta, adres_producenta, nip_producenta, data_umowy_z_producentem)
	VALUES 
    (1, "Nazwa producenta 1", "Adres producenta 1", 1111111111, "2020-01-01"),
    (2, "Nazwa producenta 2", "Adres producenta 2", 2222222222, "2020-02-01"),
    (3, "Nazwa producenta 3", "Adres producenta 3", 3333333333, "2020-03-01"),
    (4, "Nazwa producenta 4", "Adres producenta 4", 4444444444, "2020-04-01");
    
-- SELECT * FROM producenci;

-- Tabela „Produkty” – 20 pozycji

INSERT INTO produkty (produkt_id, producent_id, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, procent_VAT_sprzedazy, data_umowy_z_producentem)
	VALUES 
		(1, 1, "Nazwa produktu 1", "Opis produktu 1", 1, 1.23*1, 1.5, 0.23, "2020-01-01"),
		(2, 2, "Nazwa produktu 2", "Opis produktu 2", 2, 1.23*2, 2.5, 0.23, "2020-01-02"),
		(3, 3, "Nazwa produktu 3", "Opis produktu 3", 3, 1.23*3, 3.5, 0.23, "2020-01-03"),
		(4, 4, "Nazwa produktu 4", "Opis produktu 4", 4, 1.23*4, 4.5, 0.23, "2020-01-04"),
		(5, 1, "Nazwa produktu 5", "Opis produktu 5", 5, 1.23*5, 5.5, 0.23, "2020-01-05"),
		(6, 2, "Nazwa produktu 6", "Opis produktu 6", 6, 1.23*6, 6.5, 0.23, "2020-01-06"),
		(7, 3, "Nazwa produktu 7", "Opis produktu 7", 7, 1.23*7, 7.5, 0.23, "2020-01-07"),
		(8, 4, "Nazwa produktu 8", "Opis produktu 8", 8, 1.23*8, 8.5, 0.23, "2020-01-08"),
		(9, 1, "Nazwa produktu 9", "Opis produktu 9", 9, 1.23*9, 9.5, 0.23, "2020-01-09"),
		(10, 2, "Nazwa produktu 10", "Opis produktu 10", 10, 1.23*10, 10.5, 0.23, "2020-01-10"),
		(11, 3, "Nazwa produktu 11", "Opis produktu 11", 11, 1.23*11, 11.5, 0.23, "2020-01-11"),
		(12, 4, "Nazwa produktu 12", "Opis produktu 12", 12, 1.23*12, 12.5, 0.23, "2020-01-12"),
		(13, 1, "Nazwa produktu 13", "Opis produktu 13", 13, 1.23*13, 13.5, 0.23, "2020-01-13"),
		(14, 2, "Nazwa produktu 14", "Opis produktu 14", 14, 1.23*14, 14.5, 0.23, "2020-01-14"),
		(15, 3, "Nazwa produktu 15", "Opis produktu 15", 15, 1.23*15, 15.5, 0.23, "2020-01-15"),
		(16, 4, "Nazwa produktu 16", "Opis produktu 16", 16, 1.23*16, 16.5, 0.23, "2020-01-16"),
		(17, 1, "Nazwa produktu 17", "Opis produktu 17", 17, 1.23*17, 17.5, 0.23, "2020-01-17"),
		(18, 2, "Nazwa produktu 18", "Opis produktu 18", 18, 1.23*18, 18.5, 0.23, "2020-01-18"),
		(19, 3, "Nazwa produktu 19", "Opis produktu 19", 19, 1.23*19, 19.5, 0.23, "2020-01-19"),
		(20, 4, "Nazwa produktu 20", "Opis produktu 20", 20, 1.23*20, 20.5, 0.23, "2020-01-20");

-- SELECT * FROM produkty;

-- Tabela „Zamówienia” – 10 pozycji

INSERT INTO zamowienia (zamowienie_id, produkt_id, Data_zamowienia)
	VALUES 
		(1, 1, "2020-01-01"),
		(2, 2, "2020-02-02"),
		(3, 3, "2020-03-03"),
		(4, 4, "2020-04-04"),
		(5, 5, "2020-05-05"),
		(6, 6, "2020-06-06"),
		(7, 7, "2020-07-07"),
		(8, 8, "2020-08-08"),
		(9, 9, "2020-09-09"),
		(10, 10, "2020-10-10");

-- SELECT * FROM zamowienia;

INSERT INTO klienci (klient_id, zamowienie_id, imie, nazwisko, adres_klienta)
	VALUES 
		(1, 1, "Imię 1", "Nazwisko 1", "Adres 1"),
		(2, 2, "Imię 2", "Nazwisko 2", "Adres 2"),
		(3, 3, "Imię 3", "Nazwisko 3", "Adres 3"),
		(4, 4, "Imię 4", "Nazwisko 4", "Adres 4"),
		(5, 5, "Imię 5", "Nazwisko 5", "Adres 5"),
		(6, 6, "Imię 6", "Nazwisko 6", "Adres 6"),
		(7, 7, "Imię 7", "Nazwisko 7", "Adres 7"),
		(8, 8, "Imię 8", "Nazwisko 8", "Adres 8"),
		(9, 9, "Imię 9", "Nazwisko 9", "Adres 9"),
		(10, 10, "Imię 10", "Nazwisko 10", "Adres 10");

-- SELECT * FROM klienci;

UPDATE zamowienia SET klient_id = 1 WHERE zamowienie_id = 1;
UPDATE zamowienia SET klient_id = 2 WHERE zamowienie_id = 2;
UPDATE zamowienia SET klient_id = 3 WHERE zamowienie_id = 3;
UPDATE zamowienia SET klient_id = 4 WHERE zamowienie_id = 4;
UPDATE zamowienia SET klient_id = 5 WHERE zamowienie_id = 5;
UPDATE zamowienia SET klient_id = 6 WHERE zamowienie_id = 6;
UPDATE zamowienia SET klient_id = 7 WHERE zamowienie_id = 7;
UPDATE zamowienia SET klient_id = 8 WHERE zamowienie_id = 8;
UPDATE zamowienia SET klient_id = 9 WHERE zamowienie_id = 9;
UPDATE zamowienia SET klient_id = 10 WHERE zamowienie_id = 10;

SELECT * FROM zamowienia;

-- 8. Wyświetl wszystkie produkty z wszystkimi danymi od producenta który znajduje się na pozycji 1 w tabeli „Producenci”

SELECT * 
	FROM produkty py
    JOIN producenci pi
		ON py.producent_id = pi.producent_id
	WHERE py.producent_id = 1;

-- 9. Posortuj te produkty alfabetycznie po nazwie

SELECT * 
	FROM produkty py
    JOIN producenci pi
		ON py.producent_id = pi.producent_id
	WHERE py.producent_id = 1
	ORDER BY py.nazwa_produktu;

-- 10. Wylicz średnią cenę za produktu od producenta z pozycji 1

SELECT pi.producent_id, nazwa_produktu, AVG(cena_netto_zakupu), AVG(cena_brutto_zakupu), AVG(cena_netto_sprzedazy)
	FROM produkty py
    JOIN producenci pi
		ON py.producent_id = pi.producent_id
	WHERE py.producent_id = 1
    GROUP BY pi.producent_id, nazwa_produktu;

-- 11. Wyświetl dwie grupy produktów tego producenta:

-- Połowa najtańszych to grupa: „Tanie”
-- Pozostałe to grupa: „Drogie”

SELECT 
	nazwa_produktu,
    cena_netto_sprzedazy,
-- 	AVG(cena_netto_sprzedazy) AS AVG_CENA,
	CASE 
		WHEN cena_netto_sprzedazy > (SELECT AVG(cena_netto_sprzedazy) FROM produkty) 
        THEN "drogi" 
        ELSE "tani" 
        END grupa_produktow
    FROM produkty;
-- 	GROUP BY nazwa_produktu, cena_netto_sprzedazy, grupa_produktow;

-- SELECT AVG(cena_netto_sprzedazy) FROM produkty;

-- 12. Wyświetl produkty zamówione, wyświetlając tylko ich nazwę

SELECT nazwa_produktu
	FROM zamowienia za
    JOIN produkty py
		ON za.produkt_id = py.produkt_id
    WHERE data_zamowienia IS NOT NULL;

-- 13. Wyświetl wszystkie produkty zamówione – ograniczając wyświetlanie do 5 pozycji

-- ZADANIE NIEDOPRECYZOWANE WIĘC ZROBIŁEM W DWÓCH WARIANTACH

SELECT nazwa_produktu
	FROM zamowienia za
    JOIN produkty py
		ON za.produkt_id = py.produkt_id
    WHERE data_zamowienia IS NOT NULL
	LIMIT 5;

SELECT *
	FROM zamowienia
    WHERE data_zamowienia IS NOT NULL
    LIMIT 5;

-- 14. Policz łączną wartość wszystkich zamówień

SELECT SUM(cena_netto_sprzedazy)
	FROM produkty;

-- 15. Wyświetl wszystkie zamówienia wraz z nazwą produktu sortując je wg daty od najstarszego do najnowszego

-- V1

SELECT *
	FROM zamowienia za
    LEFT JOIN produkty py
		ON za.produkt_id = py.produkt_id
	ORDER BY za.Data_zamowienia DESC;

-- V2

SELECT zamowienie_id
	FROM zamowienia za
    LEFT JOIN produkty py
		ON za.produkt_id = py.produkt_id
	ORDER BY za.Data_zamowienia DESC;


-- 16. Sprawdź czy w tabeli produkty masz uzupełnione wszystkie dane – wyświetl pozycje dla których brakuje danych

SELECT *
	FROM produkty
	WHERE 
		produkt_id IS NULL OR
        producent_id IS NULL OR 
        nazwa_produktu IS NULL OR 
        opis_produktu IS NULL OR 
        cena_netto_zakupu IS NULL OR 
        cena_brutto_zakupu IS NULL OR 
        cena_netto_sprzedazy IS NULL OR 
        procent_VAT_sprzedazy IS NULL OR 
        data_umowy_z_producentem IS NULL;

-- 17. Wyświetl produkt najczęściej sprzedawany wraz z jego ceną

SELECT nazwa_produktu, COUNT(zamowienie_id) AS liczba_zamowien
	FROM zamowienia za
    JOIN produkty py
		ON za.produkt_id = py.produkt_id
    GROUP BY nazwa_produktu
    ORDER BY liczba_zamowien DESC
    LIMIT 1;

-- 18. Znajdź dzień w którym najwięcej zostało złożonych zamówień

SELECT Data_zamowienia, COUNT(zamowienie_id) liczba_zamowien
	FROM zamowienia
    GROUP BY Data_zamowienia
    ORDER BY liczba_zamowien DESC
    LIMIT 1;